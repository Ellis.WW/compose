package com.example.jetpackcompose

import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable

class TextViewUtils {
    @Composable
    fun TitleStyle(name: String) {
        Text(
            text = "Hello $name!",
            color = MaterialTheme.colors.secondaryVariant
        )
    }
}