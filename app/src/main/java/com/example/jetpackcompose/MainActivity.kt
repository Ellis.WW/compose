package com.example.jetpackcompose

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat.startActivity
import com.example.jetpackcompose.ui.theme.JetpackComposeTheme

class MainActivity : ComponentActivity() {
    @ExperimentalAnimationApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MaterialTheme {
                PreviewConversation()
            }
        }
    }
}

@ExperimentalAnimationApi
@Composable
fun ListDemo(strList: List<String>) {
    LazyColumn {
        items(strList.size) { message ->
            ItemUtils().MessageCard(msg = strList[message])
        }
    }
}

@ExperimentalAnimationApi
@Preview(showSystemUi = true)
@Composable
fun PreviewConversation() {
    MaterialTheme {
        ListDemo(
            arrayListOf(
                "111",
                "22222222222222222222222222222222222222\n22222222222\n22",
                "3333",
                "444",
                "5555",
                "666",
                "7777777",
                "888888888",
                "888888888",
                "888888888",
                "888888888",
                "888888888",
                "888888888",
                "888888888",
                "888888888",
                "888888888",
                "888888888",
                "888888888",
                "888888888",
                "99999",
                "aaaaaa",
                "ssssssssssssss",
                "ddddddddddddddddddddddd",
                "fffffffffffffff"
            )
        )
    }
}


@Preview(showBackground = true, showSystemUi = true)
@Preview(showBackground = true, backgroundColor = 0xFF00FF00, showSystemUi = true)
@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_YES, showSystemUi = false, name = "Dark Mode"
)
@Composable
fun DefaultPreview() {
    JetpackComposeTheme {

        Row(modifier = Modifier.padding(all = 8.dp)) {
            Image(
                painter = painterResource(R.drawable.ic_launcher_foreground),
                contentDescription = "Contact profile picture",
                modifier = Modifier
                    // Set image size to 40 dp
                    .size(40.dp)
                    // Clip image to be shaped as a circle
                    .clip(CircleShape)
                    .border(1.5.dp, MaterialTheme.colors.secondary, CircleShape)
            )
            Column(modifier = Modifier.padding(horizontal = 10.dp)) {
                TextViewUtils().TitleStyle(name = "Android")
                TextViewUtils().TitleStyle(name = "Android")
            }
        }

    }
}